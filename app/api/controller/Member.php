<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\api\controller;

use app\api\service\MemberService;
use app\api\model\Member as MemberModel;
use think\exception\ValidateException;
use think\facade\Log;

class Member extends Common {


	/**
	* @api {get} /Member/index 01、首页数据列表
	*/
	function index(){
		$limit  = $this->request->get('limit', 20, 'intval');
		$page   = $this->request->get('page', 1, 'intval');

		$where = [];
		$where['username'] = $this->request->get('username', '', 'serach_in');
		$where['sex'] = $this->request->get('sex', '', 'serach_in');
		$where['mobile'] = $this->request->get('mobile', '', 'serach_in');
		$where['email'] = $this->request->get('email', '', 'serach_in');

		$amount_start = $this->request->get('amount_start', '', 'serach_in');
		$amount_end = $this->request->get('amount_end', '', 'serach_in');

		$where['amount'] = ['between',[$amount_start,$amount_end]];
		$where['status'] = $this->request->get('status', '', 'serach_in');
		$where['province'] = $this->request->get('province', '', 'serach_in');
		$where['city'] = $this->request->get('city', '', 'serach_in');
		$where['district'] = $this->request->get('district', '', 'serach_in');
		$where['tag'] = $this->request->get('tag', '', 'serach_in');

		$create_time_start = $this->request->get('create_time_start', '', 'serach_in');
		$create_time_end = $this->request->get('create_time_end', '', 'serach_in');

		$where['create_time'] = ['between',[strtotime($create_time_start),strtotime($create_time_end)]];

		$field = 'username,sex,mobile,email,headimg';
		$orderby = 'member_id desc';

		$res = MemberService::indexList(formatWhere($where),$field,$orderby,$limit,$page);
		return $this->ajaxReturn($this->successCode,'返回成功',htmlOutList($res));
	}

	/**
	* @api {post} /Member/add 02、添加
	*/
	function add(){
		$postField = 'username,sex,mobile,email,headimg,amount,status,province,city,district,tag,create_time,password';
		$data = $this->request->only(explode(',',$postField),'post',null);
		$res = MemberService::add($data);
		return $this->ajaxReturn($this->successCode,'操作成功',$res);
	}

	/**
	* @api {post} /Member/update 03、修改
	*/
	function update(){
		$postField = 'member_id,username,sex,mobile,email,headimg,amount,status,province,city,district,tag,create_time';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(empty($data['member_id'])){
			throw new ValidateException('参数错误');
		}
		$where['member_id'] = $data['member_id'];
		$res = MemberService::update($where,$data);
		return $this->ajaxReturn($this->successCode,'操作成功');
	}

	/**
	* @api {post} /Member/delete 04、删除
	*/
	function delete(){
		$idx =  $this->request->post('member_ids', '', 'serach_in');
		if(empty($idx)){
			throw new ValidateException('参数错误');
		}
		$data['member_id'] = explode(',',$idx);
		try{
			MemberModel::destroy($data);
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return $this->ajaxReturn($this->successCode,'操作成功');
	}

	/**
	* @api {get} /Member/view 05、查看详情
	*/
	function view(){
		$data['member_id'] = $this->request->get('member_id','','serach_in');
		$field='member_id,username,sex,mobile,email,headimg,amount';
		$res  = checkData(MemberModel::field($field)->where($data)->find());
		return $this->ajaxReturn($this->successCode,'返回成功',$res);
	}

	/**
	* @api {post} /Member/recharge 08、充值
	*/
	function recharge(){
		$postField = 'member_id,amount';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(empty($data['member_id'])){
			throw new ValidateException('参数错误');
		}
		$where['member_id'] = (int) $data['member_id'];
		$res = MemberService::recharge($where,$data);
		return $this->ajaxReturn($this->successCode,'操作成功');
	}

	/**
	* @api {post} /Member/backrecharge 09、回收
	*/
	function backrecharge(){
		$postField = 'member_id,amount';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(empty($data['member_id'])){
			throw new ValidateException('参数错误');
		}
		$where['member_id'] = (int) $data['member_id'];
		$res = MemberService::backrecharge($where,$data);
		return $this->ajaxReturn($this->successCode,'操作成功');
	}

	/**
	* @api {post} /Member/uppass 10、重置密码
	*/
	function uppass(){
		$postField = 'member_id,password,repassword';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(empty($data['member_id'])){
			throw new ValidateException('参数错误');
		}
		if(empty($data['password'])){ 
			throw new ValidateException('密码不能为空');
		}
		if($data['password'] <> $data['repassword']){ 
			throw new ValidateException('两次密码输入不一致');
		}
		$where['member_id'] = $data['member_id'];
		$res = MemberService::uppass($where,$data);
		return $this->ajaxReturn($this->successCode,'操作成功');
	}



}

