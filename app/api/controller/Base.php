<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */
 
namespace app\api\controller;
use think\facade\Validate;
use think\facade\Filesystem;
use think\Image;
use think\exception\ValidateException;


class Base extends Common{
	
	
	/**
	* @api {post} /Base/upload 01、图片上传
	*/
	public function upload(){
		if(!$_FILES){
			return json(['status'=>config('my.errorCode'),'msg'=>'图片上传为空']);
		}
		$file = $this->request->file(array_keys($_FILES)[0]);
		$upload_config_id = $this->request->param('upload_config_id','','intval'); //上传配置id
		try{
			$file_type = upload_replace(config('my.api_upload_ext')); //上传黑名单检测
			if(!Validate::fileExt($file,$file_type) || !Validate::fileSize($file,config('my.api_upload_max'))){
				throw new ValidateException('上传验证失败');
			}
			//检测图片路径已存在  true 检测 读取已有的图片路径 false不检测 每次都重新上传新的
			$upload_hash_status = !is_null(config('my.upload_hash_status')) ? config('my.upload_hash_status') : true; 
			$fileinfo = db("file")->where('hash',$file->hash('md5'))->find();
			if($fileinfo && $upload_hash_status){
				if(!config('my.oss_status')){
					$url = !file_exists('.'.$fileinfo['filepath']) ? $this->up($file) : $fileinfo['filepath'];
				}else{
					$url = $fileinfo['filepath'];
				}
			}else{
				$url = $this->up($file);
			}
		}catch(\Exception $e){
			return json(['status'=>config('my.errorCode'),'msg'=>'上传失败']);
		}

		return json(['status'=>config('my.successCode'),'data'=>$url]);
	}
	
	protected function up($file){
		try{
			if(config('my.oss_status')){
				$url = \utils\oss\OssService::OssUpload(['tmp_name'=>$file->getPathname(),'extension'=>$file->extension()]);
			}else{
				$info = Filesystem::disk('public')->putFile(\utils\oss\OssService::setFilepath(),$file,'uniqid');
				$url = \utils\oss\OssService::getApiFileName(basename($info));
			}
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}
		$upload_hash_status = !is_null(config('my.upload_hash_status')) ? config('my.upload_hash_status') : true; 
		$upload_hash_status && db('file')->insert(['filepath'=>$url,'hash'=>$file->hash('md5'),'create_time'=>time()]);
		return $url;
	}

}

