<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\api\middleware;

class CaptchaAuth
{
	
    public function handle($request, \Closure $next)
    {	
		$captcha	= $request->param('captcha','','strip_tags,trim');	//验证码
		if(empty($captcha)){
			return json(['status'=>config('my.errorCode'),'msg'=>'验证码不能为空']);
		}
		
		if(!captcha_check($captcha)){
			return json(['status'=>config('my.errorCode'),'msg'=>'验证码错误']);
		}
		
		return $next($request);	
    }
} 