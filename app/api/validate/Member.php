<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\api\validate;
use think\validate;

class Member extends validate {


	protected $rule = [
		'username'=>['require'],
		'mobile'=>['require','unique:member','regex'=>'/^1[345678]\d{9}$/'],
		'email'=>['regex'=>'/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/'],
		'amount'=>['regex'=>'/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/'],
		'province'=>['require'],'city'=>['require'],
	];

	protected $message = [
		'username.require'=>'用户名不能为空',
		'mobile.require'=>'手机号不能为空',
		'mobile.unique'=>'手机号已经存在',
		'mobile.regex'=>'手机号格式错误',
		'email.regex'=>'邮箱格式错误',
		'amount.regex'=>'积分格式错误',
		'province.require'=>'所属省不能为空',
		'city.require'=>'所属市不能为空',
	];

	protected $scene  = [
		'add'=>['username','mobile','email','amount','province','city','district'],
		'update'=>['username','mobile','email','amount','province','city','district'],
		'recharge'=>['amount'],
		'backrecharge'=>['amount'],
	];



}

