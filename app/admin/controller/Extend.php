<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\admin\controller;

use app\admin\service\ExtendService;
use app\admin\model\Extend as ExtendModel;

class Extend extends Admin {


	/*首页数据列表*/
	function index(){
		if (!$this->request->isAjax()){
			return view('index');
		}else{
			$limit  = $this->request->post('limit', 20, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where = [];
			$where['title'] = $this->request->param('title', '', 'serach_in');
			$where['table_name'] = $this->request->param('table_name', '', 'serach_in');
			$where['type'] = $this->request->param('type', '', 'serach_in');
			$where['status'] = $this->request->param('status', '', 'serach_in');

			$order  = $this->request->post('order', '', 'serach_in');	//排序字段 bootstrap-table 传入
			$sort  = $this->request->post('sort', '', 'serach_in');		//排序方式 desc 或 asc

			$field = 'extend_id,title,table_name,type,status,is_post,sortid';
			$orderby = ($sort && $order) ? $sort.' '.$order : 'extend_id desc';

			$res = ExtendService::indexList(formatWhere($where),$field,$orderby,$limit,$page);
			return json($res);
		}
	}

	/*修改排序开关按钮操作*/
	function updateExt(){
		$postField = 'extend_id,status,is_post,sortid';
		$data = $this->request->only(explode(',',$postField),'post',null);
		if(!$data['extend_id']) $this->error('参数错误');
		try{
			ExtendModel::update($data);
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*添加*/
	function add(){
		if (!$this->request->isPost()){
			return view('info');
		}else{
			$postField = 'sortid,is_post,status,order_by,action,type,table_name,title';
			$data = $this->request->only(explode(',',$postField),'post',null);
			$res = ExtendService::add($data);
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}

	/*修改*/
	function update(){
		if (!$this->request->isPost()){
			$extend_id = $this->request->get('extend_id','','serach_in');
			if(!$extend_id) $this->error('参数错误');
			$this->view->assign('info',checkData(ExtendModel::find($extend_id)));
			return view('info');
		}else{
			$postField = 'extend_id,sortid,is_post,status,order_by,action,type,table_name,title';
			$data = $this->request->only(explode(',',$postField),'post',null);
			$res = ExtendService::update($data);
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('extend_id', '', 'serach_in');
		if(!$idx) $this->error('参数错误');
		try{
			ExtendModel::destroy(['extend_id'=>explode(',',$idx)]);
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}



}

