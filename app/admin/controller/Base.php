<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */
 
namespace app\admin\controller;
use app\admin\service\UserService;
use app\admin\service\RoleService;
use app\admin\service\Cms\CatagoryService;
use app\admin\model\User;
use app\admin\model\Role;

class Base extends Admin
{
   
	//系统配置
    function config(){
		if (!$this->request->isPost()){
			$info = db("config")->column('data','name');
			$this->view->assign('info',checkData($info,false));
			$this->view->assign('themesList',CatagoryService::tplList(''));
			return view('base/config');
		}else{
			$data = $this->request->post();
			try{
				$info = db("config")->column('data','name');
				foreach ($data as $key => $value) {
					if(array_key_exists($key,$info)){
						db("config")->where(['name'=>$key])->update(['data'=>$value]);
					}else{
						db("config")->insert(['name'=>$key,'data'=>$value]);
					}
				}
			}catch(\Exception $e){
				abort(config('error_log_code'),$e->getMessage());
			}
            return json(['status'=>'00','msg'=>'修改成功']);
		}
	}
	
    //修改密码
    public function password(){
	    if (!$this->request->isPost()){	
			return view('password');
		}else{
			$password = $this->request->post('password','','trim');
			$data['user_id'] = session('admin.user_id');
			$data['pwd'] = md5($password.config('my.password_secrect'));
			try{
				User::update($data);
			}catch(\Exception $e){
				abort(config('error_log_code'),$e->getMessage());
			}
            return json(['status'=>'00','message'=>'修改成功']);
		}
    }
	
	
	public function auth(){
		if (!$this->request->isAjax()){
			$role_id = $this->request->get('role_id','','intval');
			$info = Role::find($role_id);
			$parentInfo = Role::find($info->pid);
			$parentAccess = db("access")->where('role_id',$parentInfo->role_id)->column('purviewval','id');	//父角色俱备的权限
			$access = db("access")->where('role_id',$role_id)->column('purviewval','id');	//当前角色的权限
			$nodes = $this->getNodes(0,$access,$parentAccess,$info);
			return view('auth',['nodes'=>json_encode($nodes)]);
		}else{
			$role_id = $this->request->post('role_id','','intval');
			$purval = $this->request->post('idx','','strval');
			db('access')->where('role_id',$role_id)->delete();
			foreach($purval as $val){
				$data = ['purviewval'=>$val,'role_id'=>$role_id];				
				db('access')->insert($data);								
			}
			return json(['status'=>'00','message'=>'设置成功']);
		}
	}
	
	public function getNodes($pid,$access,$parentAccess,$info){
		$list = db("node")->where('pid',$pid)->order('sortid asc')->select()->toArray();
		if($list){
			foreach($list as $key=>$val){
				$selectStatus = false;
				$url = $val['val'];
				if(in_array($url,$access)){
					$selectStatus = true;
				}
				if(in_array($url,$parentAccess) || empty($info['pid']) || $info['pid'] == 1){
					$menus[$key]['text'] = $val['title'].' '.$url;
					$menus[$key]['state'] = ['opened'=>true,'selected'=>$selectStatus];
					$menus[$key]['a_attr'] = ['data-id'=>$url];
					$sublist = db("node")->where('pid',$val['id'])->order('sortid asc')->select()->toArray();
					if($sublist){
						$menus[$key]['children'] = $this->getNodes($val['id'],$access,$parentAccess,$info);
					}
					if($val['id'] == 258){
						$menus[$key]['children'] = $this->getFormList($access,$parentAccess,$info);
					}
				}
			}
		}
		return array_values($menus);
	}
	
	
	//获取表单菜单
	public function getFormList($access,$parentAccess,$info){
		$menulist = db("extend")->where(['status'=>1,'type'=>2])->order('sortid desc,extend_id desc')->select()->toArray();
		foreach($menulist as $key=>$val){
			$selectStatus = false;
			$url = '/admin/FormData/index.html?extend_id='.$val['extend_id'];
			if(in_array($url,$access)){
				$selectStatus = true;
			}
			if(in_array($url,$parentAccess) || empty($info['pid']) || $info['pid'] == 1){
				$menu[$key]['text'] = $val['title'].' '.$url;
				$menu[$key]['state'] = ['opened'=>true,'selected'=>$selectStatus];
				$menu[$key]['a_attr'] = ['data-id'=>$url];
			}
		}
		return array_values($menu);
	}
	
	//清除缓存 出去session缓存
	public function clearData(){
		try{
			deldir(app()->getRootPath().'runtime/admin');
			deldir(app()->getRootPath().'runtime/index');
		}catch(\Exception $e){
			return json(['status'=>'01','msg'=>$e->getMessage()]);
		}
		return json(['status'=>'00','msg'=>'删除成功']);
	}
	
	//字体图标选择器
    public function icon(){
        $field =input('param.field','','strval');
		$this->view->assign('field',$field);
		return view('icon');
    }
	
	

}
