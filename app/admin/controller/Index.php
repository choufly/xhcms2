<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */
 
namespace app\admin\controller;

class Index extends Admin
{
	
	//框架主体
    public function index(){
		$this->view->assign('menus',$this->getSubMenu(0));
		return view('index');
    }
	
	//生成左侧菜单栏
	private function getSubMenu($pid){
		$list = db("node")->where(['status'=>1,'is_menu'=>1,'pid'=>$pid])->order('sortid asc')->select()->toArray();
		if($list){
			foreach($list as $key=>$val){
				$sublist = db("node")->where(['status'=>1,'is_menu'=>1,'pid'=>$val['id']])->order('sortid asc')->select()->toArray();
				if($sublist){
					$menus[$key]['sub'] = $this->getSubMenu($val['id']);
				}

				if($val['id'] == 258){
					$menus[$key]['sub'] = $this->getFormList();
				}
				$menus[$key]['title'] = $val['title'];
				$menus[$key]['icon'] = !empty($val['icon']) ? $val['icon'] : 'fa fa-clone';
				$menus[$key]['url'] = url($val['val']);
				$menus[$key]['access_url'] = $val['val'];
			}
			return $menus;
		}
	}
	
	//获取表单菜单
	public function getFormList(){
		$menulist = db("extend")->where(['status'=>1,'type'=>2])->order('sortid desc,extend_id desc')->select()->toArray();
		foreach($menulist as $key=>$val){
			$menu[$key]['title'] = $val['title'];
			$menu[$key]['icon'] = 'fa fa-clone';
			$menu[$key]['url'] = url('admin/FormData/index',['extend_id'=>$val['extend_id']]);	
			$menu[$key]['access_url'] = '/admin/FormData/index.html?extend_id='.$val['extend_id'];	
		}
		return $menu;
	}
	
	//后台首页框架内容
	public function main(){	
		return view('main');
	}
	
}
