<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */
 
 
namespace app\admin\controller;
use app\admin\service\FieldService;
use app\admin\service\FieldSetService;
use app\admin\controller\Admin;

class Field extends Admin
{
	
    public function index(){
	   if (!$this->request->isAjax()){
			return view('index');
		}else{
			$limit  = input('post.limit', 20, 'intval');
			$offset = input('post.offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;
			
			$extend_id = $this->request->get('extend_id','','intval');
			$res = db('field')->where(['extend_id'=>$extend_id])->order('sortid asc')->paginate(['list_rows'=>$limit,'page'=>$page]);
			$list = $res->items();
			foreach($list as $key=>$val){
				$typeField = FieldSetService::typeField();
				$propertyField = FieldSetService::propertyField();
				$typeData = $typeField[$val['type']];
				$property = $propertyField[$typeData['property']];
				if($property['name'] == 'decimal'){
					$list[$key]['datatype'] = $property['name'].'('.$property['maxlen'].','.$property['decimal'].')';
				}else{
					$list[$key]['datatype'] = $property['name'].'('.$property['maxlen'].')';
				}
				$list[$key]['type'] = $typeData['name'];				
			}
			$data['total'] = $res->total();
			$data['rows']  = $list;
			return json($data);
		}
    }
	
	public function add(){
		if (!$this->request->isPost()){
			$extend_id = $this->request->get('extend_id','','intval');
			$this->view->assign('fieldList',FieldSetService::typeField());
			$this->view->assign('ruleList',FieldSetService::ruleList());
			$this->view->assign('propertyList',FieldSetService::propertyField());
			$this->view->assign('extend_id',$extend_id);
			return view('info');
		}else{
			$data = $this->request->post();
			FieldService::saveData('add',$data);
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}
	
	public function update(){
		if (!$this->request->isPost()){
			$id = $this->request->get('id','','intval');
			if(!$id) $this->error('参数错误');
			$info = db('field')->where('id',$id)->find();
			$this->view->assign('fieldList',FieldSetService::typeField());
			$this->view->assign('ruleList',FieldSetService::ruleList());
			$this->view->assign('propertyList',FieldSetService::propertyField());
			$this->view->assign('extend_id',$info['extend_id']);
			$this->view->assign('info',$info);
			return view('info');
		}else{
			$data = $this->request->post();
			FieldService::saveData('edit',$data);
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}
	
	//更新排序
	public function setSort(){
		$id = $this->request->post('id','','intval');
		$sortid = $this->request->post('sortid','','intval');
		if(!$id) $this->error('参数错误');
		try{
			db('field')->update(['id'=>$id,'sortid'=>$sortid]);
		} catch (\Exception $e) {
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return json(['status'=>'00','msg'=>'修改成功']);
	}
	
	
	//箭头排序
	public function arrowsort(){
		$id = $this->request->post('id','','intval');
		$type = $this->request->post('type',1,'intval');		
		if(empty($id)) $this->error('参数错误');	
		try{
			FieldService::arrowsort($id,$type);
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return json(['status'=>'00','msg'=>'设置成功']);
	}
	
	public function delete(){
		$id = $this->request->post('id','','intval');
		if(!$id) $this->error('参数错误');
		try{
			db('field')->delete($id);
		} catch (\Exception $e) {
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return json(['status'=>'00','msg'=>'删除成功']);
	}
	
	
	/*修改排序、开关按钮操作 如果没有此类操作 可以删除该方法*/
	function updateExt(){
		$data = $this->request->post();
		if(!$data['id']) $this->error('参数错误');
		try{
			db('field')->update($data);
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}
	
	
}
