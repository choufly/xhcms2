<?php

return [
	'alias' => [
        'SetTable' => app\admin\middleware\SetTable::class,
		'UpTable'  => app\admin\middleware\UpTable::class,
		'SetField'  => app\admin\middleware\SetField::class,
		'UpField'  => app\admin\middleware\UpField::class,
		'DeleteField'  => app\admin\middleware\DeleteField::class,
		'DeleteMenu'  => app\admin\middleware\DeleteMenu::class,
    ],
];
