<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\admin\service;
use app\admin\model\User;
use think\exception\ValidateException;
use xhadmin\CommonService;

class UserService extends CommonService {


	/*
 	* @Description  添加账户
 	*/
	public static function add($data){
		try{
			validate(\app\admin\validate\User::class)->scene('add')->check($data);
			$data['pwd'] = md5($data['pwd'].config('my.password_secrect'));
			$data['role_id'] = implode(',',$data['role_id']);
			$data['create_time'] = time();
			$res = User::create($data);
		}catch(ValidateException $e){
			throw new ValidateException ($e->getError());
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return $res->user_id;
	}


	/*
 	* @Description  修改账户
 	*/
	public static function update($data){
		try{
			validate(\app\admin\validate\User::class)->scene('update')->check($data);
			$data['role_id'] = implode(',',$data['role_id']);
			$data['create_time'] = strtotime($data['create_time']);
			$res = User::update($data);
		}catch(ValidateException $e){
			throw new ValidateException ($e->getError());
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return $res;
	}


	/*
 	* @Description  修改密码
 	*/
	public static function updatePassword($data){
		try{
			validate(\app\admin\validate\User::class)->scene('updatePassword')->check($data);
			$data['pwd'] = md5($data['pwd'].config('my.password_secrect'));
			$res = User::update($data);
		}catch(ValidateException $e){
			throw new ValidateException ($e->getError());
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		return $res;
	}




}

